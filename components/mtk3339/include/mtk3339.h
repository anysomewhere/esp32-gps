#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

#include "nmea_parser.h"

nmea_parser_handle_t mtk3339_init(const nmea_parser_config_t *config);
void parse_pmtk001(gps_t *gps, nmea_parser_state_t *state);

#ifdef __cplusplus
}
#endif
