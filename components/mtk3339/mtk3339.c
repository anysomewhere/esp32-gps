#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "esp_log.h"
#include "esp_types.h"
#include "mtk3339.h"
#include "nmea_parser.h"

#define PMTK_SET_NMEA_UPDATE_100_MILLIHERTZ "$PMTK220,10000*2F" ///< Once every 10 seconds, 100 millihertz.
#define PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ "$PMTK220,5000*1B"  ///< Once every 5 seconds, 200 millihertz.
#define PMTK_SET_NMEA_UPDATE_1HZ "$PMTK220,1000*1F"             ///<  1 Hz
#define PMTK_SET_NMEA_UPDATE_2HZ "$PMTK220,500*2B"              ///<  2 Hz
#define PMTK_SET_NMEA_UPDATE_5HZ "$PMTK220,200*2C"              ///<  5 Hz
#define PMTK_SET_NMEA_UPDATE_10HZ "$PMTK220,100*2F"             ///< 10 Hz
// Position fix update rate commands.
#define PMTK_API_SET_FIX_CTL_100_MILLIHERTZ "$PMTK300,10000,0,0,0,0*2C" ///< Once every 10 seconds, 100 millihertz.
#define PMTK_API_SET_FIX_CTL_200_MILLIHERTZ "$PMTK300,5000,0,0,0,0*18"  ///< Once every 5 seconds, 200 millihertz.
#define PMTK_API_SET_FIX_CTL_1HZ "$PMTK300,1000,0,0,0,0*1C"             ///< 1 Hz
#define PMTK_API_SET_FIX_CTL_5HZ "$PMTK300,200,0,0,0,0*2F"              ///< 5 Hz
// Can't fix position faster than 5 times a second!

static const char *MTK3339_TAG = "mtk3339";

typedef struct
{
  uart_port_t uart_port;                         /*!< Uart port number */
} esp_mtk3339_t;

/**
 * @brief Define MTK3339 commands
 * 
 */
typedef enum
{
  PMTK_NONE = -1,
  PMTK_TEST = 0,
  PMTK_SET_NMEA_BAUDRATE = 251
} mtk3339_command_t;

typedef enum
{
  NONE = -1,
  COMMAND_INVALID = 0,
  COMMAND_UNSUPPORTED = 1,
  ACTION_FAILED = 2,
  ACTION_SUCCEEDED = 3
} mtk3339_ack_flag_t;

typedef struct {
  mtk3339_command_t command;
  mtk3339_ack_flag_t flag;
} mtk3339_ack_state_t;

static mtk3339_ack_state_t mtk3339_ack_state;

/**
 * @brief Send NMEA sentence to MTL3339
 * 
 * @param handle 
 */

static esp_err_t mtk3339_send_sentence(uart_port_t uart_port, mtk3339_command_t cmd, const char *value)
{
  uint8_t checksum = 0;
  char sentence[53];

  memset(sentence, 0, sizeof(sentence));
  sprintf(sentence, "$PMTK%03u", cmd);
  if (value)
  {
    strcat(sentence, ",");
    strcat(sentence, value);
  }
  strcat(sentence, "*");

  for (int i = 0; i < strlen(sentence); i++)
  {
    char c = sentence[i];
    if (c == '$' || c == '*')
      continue;
    checksum ^= c;
  }
  sprintf(&(sentence[strlen(sentence)]), "%x", checksum);
  strcat(sentence, "\r\n");

  ESP_LOGI(MTK3339_TAG, "Sending command: %s", sentence);
  uart_write_bytes(uart_port, sentence, strlen(sentence));

  return ESP_OK;
}

/**
 * @brief Parse PMTK001 statements
 *
 * @param state nmea_parser_state_t type object
 */
static void mtk3339_parse_pmtk001(nmea_parser_state_t *state)
{
  mtk3339_ack_state_t *ack = (mtk3339_ack_state_t *)state->context;
  switch (state->item_num) {
  case 0:
    ack->command = PMTK_NONE;
    ack->flag = NONE;
    break;
  case 1:
    ack->command = (mtk3339_command_t)strtol(state->item_str, NULL, 10);
    ESP_LOGD(MTK3339_TAG, "Ack command: %d", ack->command);
    break;
  case 2:
    ack->flag = (mtk3339_ack_flag_t)strtol(state->item_str, NULL, 10);
    ESP_LOGD(MTK3339_TAG, "Ack flag: %d", ack->flag);
    break;
  default:
    break;
  }
}

/**
 * @brief Init MTK3339
 * 
 * @param config Configuration of NMEA Parser
 * @return nmea_parser_handle_t handle of NMEA parser
 */
nmea_parser_handle_t mtk3339_init(const nmea_parser_config_t *config)
{
  esp_mtk3339_t *esp_mtk = nmea_parser_init(config);

  nmea_parser_register_function(esp_mtk, &(nmea_parser_handler_t){ "$PMTK001", &mtk3339_parse_pmtk001, false, NULL, &mtk3339_ack_state });

  mtk3339_send_sentence(esp_mtk->uart_port, PMTK_TEST, NULL);

  return esp_mtk;
}
