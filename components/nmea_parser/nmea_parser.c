#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "nmea_parser.h"

#ifdef CONFIG_NMEA_STATEMENT_GGA
  #define NMEA_STATEMENT_GGA_REQUIRED true
#else
  #define NMEA_STATEMENT_GGA_REQUIRED false
#endif
#ifdef CONFIG_NMEA_STATEMENT_GSA
  #define NMEA_STATEMENT_GSA_REQUIRED true
#else
  #define NMEA_STATEMENT_GSA_REQUIRED false
#endif
#ifdef CONFIG_NMEA_STATEMENT_RMC
  #define NMEA_STATEMENT_RMC_REQUIRED true
#else
  #define NMEA_STATEMENT_RMC_REQUIRED false
#endif
#ifdef CONFIG_NMEA_STATEMENT_GSV
  #define NMEA_STATEMENT_GSV_REQUIRED true
#else
  #define NMEA_STATEMENT_GSV_REQUIRED false
#endif
#ifdef CONFIG_NMEA_STATEMENT_GLL
  #define NMEA_STATEMENT_GLL_REQUIRED true
#else
  #define NMEA_STATEMENT_GLL_REQUIRED false
#endif
#ifdef CONFIG_NMEA_STATEMENT_VTG
  #define NMEA_STATEMENT_VTG_REQUIRED true
#else
  #define NMEA_STATEMENT_VTG_REQUIRED false
#endif

/**
 * @brief NMEA Parser runtime buffer size
 *
 */
#define NMEA_PARSER_RUNTIME_BUFFER_SIZE (CONFIG_NMEA_PARSER_RING_BUFFER_SIZE / 2)
#define NMEA_EVENT_LOOP_QUEUE_SIZE (16)

/**
 * @brief Define of NMEA Parser Event base
 *
 */
ESP_EVENT_DEFINE_BASE(ESP_NMEA_EVENT);

static const char *GPS_TAG = "nmea_parser";

typedef struct
{
  uint8_t sat_num;                               /*!< Satellite number */
  uint8_t sat_count;                             /*!< Satellite count */
} sat_t;

/**
 * @brief GPS parser library runtime structure
 *
 */
typedef struct {
  uart_port_t uart_port;                         /*!< Uart port number */
  uint32_t parsed_statement;                     /*!< OR'd of statements that have been parsed */
  uint32_t all_statements;                       /*!< All statements mask */
  gps_t data;                                    /*!< Parent class */
  sat_t sat;                                     /*!< Satellite data */
  uint8_t *buffer;                               /*!< Runtime buffer */
  esp_event_loop_handle_t event_loop_handle;     /*!< Event loop handle */
  TaskHandle_t task_handle;                      /*!< NMEA Parser task handle */
  QueueHandle_t event_queue;                     /*!< UART event queue handle */
  nmea_parser_handler_t *handlers[255];
} esp_gps_t;

/**
 * @brief parse latitude or longitude
 *              format of latitude in NMEA is ddmm.sss and longitude is dddmm.sss
 * @param state nmea_parser_state_t type object
 * @return float Latitude or Longitude value (unit: degree)
 */
static float parse_lat_long(nmea_parser_state_t *state)
{
  float ll = strtof(state->item_str, NULL);
  int deg = ((int)ll) / 100;
  float min = ll - (deg * 100);
  ll = deg + min / 60.0f;
  return ll;
}

/**
 * @brief Converter two continuous numeric character into a uint8_t number
 *
 * @param digit_char numeric character
 * @return uint8_t result of converting
 */
static inline uint8_t convert_two_digit2number(const char *digit_char)
{
  return 10 * (digit_char[0] - '0') + (digit_char[1] - '0');
}

/**
 * @brief Parse UTC time in GPS statements
 *
 * @param state nmea_parser_state_t type object
 */
static void parse_utc_time(nmea_parser_state_t *state)
{
  gps_t *gps = (gps_t *)state->object;
  gps->time.hour = convert_two_digit2number(state->item_str + 0);
  gps->time.minute = convert_two_digit2number(state->item_str + 2);
  gps->time.second = convert_two_digit2number(state->item_str + 4);
  if (state->item_str[6] == '.') {
    uint16_t tmp = 0;
    uint8_t i = 7;
    while (state->item_str[i]) {
      tmp = 10 * tmp + state->item_str[i] - '0';
      i++;
    }
    gps->time.thousand = tmp;
  }
}

/**
 * @brief Parse GGA statements
 *
 * @param state nmea_parser_state_t type object
 */
static void nmea_parse_gga(nmea_parser_state_t *state)
{
  gps_t *gps = (gps_t *)state->object;
  /* Process GGA statement */
  switch (state->item_num) {
  case 1: /* Process UTC time */
    parse_utc_time(state);
    break;
  case 2: /* Latitude */
    gps->latitude = parse_lat_long(state);
    break;
  case 3: /* Latitude north(1)/south(-1) information */
    if (state->item_str[0] == 'S' || state->item_str[0] == 's') {
      gps->latitude *= -1;
    }
    break;
  case 4: /* Longitude */
    gps->longitude = parse_lat_long(state);
    break;
  case 5: /* Longitude east(1)/west(-1) information */
    if (state->item_str[0] == 'W' || state->item_str[0] == 'w') {
      gps->longitude *= -1;
    }
    break;
  case 6: /* Fix status */
    gps->fix = (gps_fix_t)strtol(state->item_str, NULL, 10);
    break;
  case 7: /* Satellites in use */
    gps->sats_in_use = (uint8_t)strtol(state->item_str, NULL, 10);
    break;
  case 8: /* HDOP */
    gps->dop_h = strtof(state->item_str, NULL);
    break;
  case 9: /* Altitude */
    gps->altitude = strtof(state->item_str, NULL);
    break;
  case 11: /* Altitude above ellipsoid */
    gps->altitude += strtof(state->item_str, NULL);
    break;
  default:
    break;
  }
}

/**
 * @brief Parse GSA statements
 *
 * @param state nmea_parser_state_t type object
 */
static void nmea_parse_gsa(nmea_parser_state_t *state)
{
  gps_t *gps = (gps_t *)state->object;
  /* Process GSA statement */
  switch (state->item_num) {
  case 2: /* Process fix mode */
    gps->fix_mode = (gps_fix_mode_t)strtol(state->item_str, NULL, 10);
    break;
  case 15: /* Process PDOP */
    gps->dop_p = strtof(state->item_str, NULL);
    break;
  case 16: /* Process HDOP */
    gps->dop_h = strtof(state->item_str, NULL);
    break;
  case 17: /* Process VDOP */
    gps->dop_v = strtof(state->item_str, NULL);
    break;
  default:
    /* Parse satellite IDs */
    if (state->item_num >= 3 && state->item_num <= 14) {
        gps->sats_id_in_use[state->item_num - 3] = (uint8_t)strtol(state->item_str, NULL, 10);
    }
    break;
  }
}

/**
 * @brief Parse GSV statements
 *
 * @param state nmea_parser_state_t type object
 */
static void nmea_parse_gsv(nmea_parser_state_t *state)
{
  gps_t *gps = (gps_t *)state->object;
  sat_t *sats = (sat_t *)state->context;
  /* Process GSV statement */
  switch (state->item_num) {
  case 0:
    sats->sat_count = 0;
    sats->sat_num = 0;
    break;
  case 1: /* total GSV numbers */
    sats->sat_count = (uint8_t)strtol(state->item_str, NULL, 10);
    break;
  case 2: /* Current GSV statement number */
    sats->sat_num = (uint8_t)strtol(state->item_str, NULL, 10);
    break;
  case 3: /* Process satellites in view */
    gps->sats_in_view = (uint8_t)strtol(state->item_str, NULL, 10);
    break;
  default:
    if (state->item_num >= 4 && state->item_num <= 19) {
      uint8_t item_num = state->item_num - 4; /* Normalize item number from 4-19 to 0-15 */
      uint8_t index;
      uint32_t value;
      index = 4 * (sats->sat_num - 1) + item_num / 4; /* Get array index */
      if (index < GPS_MAX_SATELLITES_IN_VIEW) {
        value = strtol(state->item_str, NULL, 10);
        switch (item_num % 4) {
        case 0:
          gps->sats_desc_in_view[index].num = (uint8_t)value;
          break;
        case 1:
          gps->sats_desc_in_view[index].elevation = (uint8_t)value;
          break;
        case 2:
          gps->sats_desc_in_view[index].azimuth = (uint16_t)value;
          break;
        case 3:
          gps->sats_desc_in_view[index].snr = (uint8_t)value;
          break;
        default:
          break;
        }
      }
    }
    break;
  }
  state->stmt_ready = sats->sat_num == sats->sat_count;
}

/**
 * @brief Parse RMC statements
 *
 * @param state nmea_parser_state_t type object
 */
static void nmea_parse_rmc(nmea_parser_state_t *state)
{
  gps_t *gps = (gps_t *)state->object;
  /* Process GPRMC statement */
  switch (state->item_num) {
  case 1:/* Process UTC time */
    parse_utc_time(state);
    break;
  case 2: /* Process valid status */
    gps->valid = (state->item_str[0] == 'A');
    break;
  case 3:/* Latitude */
    gps->latitude = parse_lat_long(state);
    break;
  case 4: /* Latitude north(1)/south(-1) information */
    if (state->item_str[0] == 'S' || state->item_str[0] == 's') {
      gps->latitude *= -1;
    }
    break;
  case 5: /* Longitude */
    gps->longitude = parse_lat_long(state);
    break;
  case 6: /* Longitude east(1)/west(-1) information */
    if (state->item_str[0] == 'W' || state->item_str[0] == 'w') {
      gps->longitude *= -1;
    }
    break;
  case 7: /* Process ground speed in unit m/s */
    gps->speed = strtof(state->item_str, NULL) * 1.852;
    break;
  case 8: /* Process true course over ground */
    gps->cog = strtof(state->item_str, NULL);
    break;
  case 9: /* Process date */
    gps->date.day = convert_two_digit2number(state->item_str + 0);
    gps->date.month = convert_two_digit2number(state->item_str + 2);
    gps->date.year = convert_two_digit2number(state->item_str + 4);
    break;
  case 10: /* Process magnetic variation */
    gps->variation = strtof(state->item_str, NULL);
    break;
  case 11: /* Magnetic variation east(1)/west(-1) information */
    if (state->item_str[0] == 'W' || state->item_str[0] == 'w') {
      gps->variation *= -1;
    }
    break;
  default:
    break;
  }
}

/**
 * @brief Parse GLL statements
 *
 * @param state nmea_parser_state_t type object
 */
static void nmea_parse_gll(nmea_parser_state_t *state)
{
  gps_t *gps = (gps_t *)state->object;
  /* Process GPGLL statement */
  switch (state->item_num) {
  case 1:/* Latitude */
    gps->latitude = parse_lat_long(state);
    break;
  case 2: /* Latitude north(1)/south(-1) information */
    if (state->item_str[0] == 'S' || state->item_str[0] == 's') {
      gps->latitude *= -1;
    }
    break;
  case 3: /* Longitude */
    gps->longitude = parse_lat_long(state);
    break;
  case 4: /* Longitude east(1)/west(-1) information */
    if (state->item_str[0] == 'W' || state->item_str[0] == 'w') {
      gps->longitude *= -1;
    }
    break;
  case 5:/* Process UTC time */
    parse_utc_time(state);
    break;
  case 6: /* Process valid status */
    gps->valid = (state->item_str[0] == 'A');
    break;
  default:
    break;
  }
}

/**
 * @brief Parse VTG statements
 *
 * @param state nmea_parser_state_t type object
 */
static void nmea_parse_vtg(nmea_parser_state_t *state)
{
  gps_t *gps = (gps_t *)state->object;
  /* Process GPVGT statement */
  switch (state->item_num) {
  case 1: /* Process true course over ground */
    gps->cog = strtof(state->item_str, NULL);
    break;
  case 3:/* Process magnetic variation */
    gps->variation = strtof(state->item_str, NULL);
    break;
  case 5:/* Process ground speed in unit m/s */
    gps->speed = strtof(state->item_str, NULL) * 1.852;//knots to m/s
    break;
  case 7:/* Process ground speed in unit m/s */
    gps->speed = strtof(state->item_str, NULL) / 3.6;//km/h to m/s
    break;
  default:
    break;
  }
}

/**
 * @brief Parse received statement
 *
 * @param functions nmea_parser_functions_t available parser functions
 * @param state nmea_parser_state_t current parser state
 * @return esp_err_t ESP_OK on success, ESP_FAIL on error
 */
nmea_parser_handler_t *parse_stmt(esp_gps_t *esp_gps, nmea_parser_state_t *state)
{
  for (uint8_t i = 0; i < 255 && esp_gps->handlers[i]; i++)
  {
    nmea_parser_handler_t *handler = esp_gps->handlers[i];
    if (strcmp(state->item_str, handler->stmt) == 0)
    {
      state->stmt_mask = (1 << i);
      state->object = handler->object;
      state->context = handler->context;
      return handler;
    }
  }

  ESP_LOGE(GPS_TAG, "No parser function found for: %s", state->item_str);

  return NULL;
}

/**
 * @brief Parse NMEA statements from GPS receiver
 *
 * @param esp_gps esp_gps_t type object
 * @param len number of bytes to decode
 * @return esp_err_t ESP_OK on success, ESP_FAIL on error
 */
static esp_err_t gps_decode(esp_gps_t *esp_gps, size_t len)
{
  const uint8_t *d = esp_gps->buffer;
  const nmea_parser_handler_t *handler = NULL;
  nmea_parser_state_t state;
  while (*d) {
    /* Start of a statement */
    if (*d == '$') {
      /* Reset runtime information */
      state.asterisk = 0;
      state.item_num = 0;
      state.item_pos = 0;
      state.crc = 0;
      state.stmt_mask = 0;
      state.stmt_ready = true;
      state.object = NULL;
      state.context = NULL;
      /* Add character to item */
      state.item_str[state.item_pos++] = *d;
      state.item_str[state.item_pos] = '\0';
    }
    /* Detect item separator character */
    else if (*d == ',') {
      if (handler == NULL)
      {
        /* Start of a statement */
        handler = parse_stmt(esp_gps, &state);
      }
      /* Parse current item */
      handler->handler(&state);

      /* Add character to CRC computation */
      state.crc ^= (uint8_t)(*d);
      /* Start with next item */
      state.item_pos = 0;
      state.item_str[0] = '\0';
      state.item_num++;
    }
    /* End of CRC computation */
    else if (*d == '*') {
      if (handler)
      {
        /* Parse current item */
        handler->handler(&state);
      }
      /* Asterisk detected */
      state.asterisk = 1;
      /* Start with next item */
      state.item_pos = 0;
      state.item_str[0] = '\0';
      state.item_num++;
    }
    /* End of statement */
    else if (*d == '\r') {
      /* Convert received CRC from string (hex) to number */
      uint8_t crc = (uint8_t)strtol(state.item_str, NULL, 16);
      /* CRC passed */
      if (state.crc == crc) {
        ESP_LOGD(GPS_TAG, "%s", esp_gps->buffer);
        if (state.stmt_ready) {
          esp_gps->parsed_statement |= state.stmt_mask;
        }
        /* Check if all statements have been parsed */
        if (((esp_gps->parsed_statement) & esp_gps->all_statements) == esp_gps->all_statements) {
          esp_gps->parsed_statement = 0;
          /* Send signal to notify that GPS information has been updated */
          esp_event_post_to(esp_gps->event_loop_handle, ESP_NMEA_EVENT, GPS_UPDATE,
                            &(esp_gps->data), sizeof(gps_t), pdMS_TO_TICKS(100));
        }
      } else {
        ESP_LOGD(GPS_TAG, "CRC Error for statement:%s", esp_gps->buffer);
      }
      if (handler == NULL) {
        /* Send signal to notify that one unknown statement has been met */
        esp_event_post_to(esp_gps->event_loop_handle, ESP_NMEA_EVENT, GPS_UNKNOWN,
                          esp_gps->buffer, len, pdMS_TO_TICKS(100));
      }
      handler = NULL;
    }
    /* Other non-space character */
    else {
      if (!(state.asterisk)) {
        /* Add to CRC */
        state.crc ^= (uint8_t)(*d);
      }
      /* Add character to item */
      state.item_str[state.item_pos++] = *d;
      state.item_str[state.item_pos] = '\0';
    }
    /* Process next character */
    d++;
  }
  return ESP_OK;
}

/**
 * @brief Handle when a pattern has been detected by uart
 *
 * @param esp_gps esp_gps_t type object
 */
static void esp_handle_uart_pattern(esp_gps_t *esp_gps)
{
  int pos = uart_pattern_pop_pos(esp_gps->uart_port);
  if (pos != -1) {
    /* read one line(include '\n') */
    int read_len = uart_read_bytes(esp_gps->uart_port, esp_gps->buffer, pos + 1, pdMS_TO_TICKS(100));
    /* make sure the line is a standard string */
    esp_gps->buffer[read_len] = '\0';
    /* Send new line to handle */
    if (gps_decode(esp_gps, read_len + 1) != ESP_OK) {
      ESP_LOGW(GPS_TAG, "GPS decode line failed");
    }
  } else {
    ESP_LOGW(GPS_TAG, "Pattern Queue Size too small");
    uart_flush_input(esp_gps->uart_port);
  }
}

/**
 * @brief NMEA Parser Task Entry
 *
 * @param arg argument
 */
static void nmea_parser_task_entry(void *arg)
{
  esp_gps_t *esp_gps = (esp_gps_t *)arg;
  uart_event_t event;
  while (1) {
    if (xQueueReceive(esp_gps->event_queue, &event, pdMS_TO_TICKS(200))) {
      switch (event.type) {
      case UART_DATA:
        break;
      case UART_FIFO_OVF:
        ESP_LOGW(GPS_TAG, "HW FIFO Overflow");
        uart_flush(esp_gps->uart_port);
        xQueueReset(esp_gps->event_queue);
        break;
      case UART_BUFFER_FULL:
        ESP_LOGW(GPS_TAG, "Ring Buffer Full");
        uart_flush(esp_gps->uart_port);
        xQueueReset(esp_gps->event_queue);
        break;
      case UART_BREAK:
        ESP_LOGW(GPS_TAG, "Rx Break");
        break;
      case UART_PARITY_ERR:
        ESP_LOGE(GPS_TAG, "Parity Error");
        break;
      case UART_FRAME_ERR:
        ESP_LOGE(GPS_TAG, "Frame Error");
        break;
      case UART_PATTERN_DET:
        esp_handle_uart_pattern(esp_gps);
        break;
      default:
        ESP_LOGW(GPS_TAG, "unknown uart event type: %d", event.type);
        break;
      }
    }
    /* Drive the event loop */
    esp_event_loop_run(esp_gps->event_loop_handle, pdMS_TO_TICKS(50));
  }
  vTaskDelete(NULL);
}

esp_err_t nmea_parser_register_function(nmea_parser_handle_t nmea_handle, const nmea_parser_handler_t *handler)
{
  esp_gps_t *esp_gps = (esp_gps_t *)nmea_handle;
  for (uint8_t i = 0; i < 255; i++)
  {
    if (esp_gps->handlers[i] == NULL)
    {
      esp_gps->handlers[i] = malloc(sizeof(nmea_parser_handler_t));
      if (esp_gps->handlers[i] == NULL) {
        ESP_LOGE(GPS_TAG, "malloc memory for lookup failed");
        return ESP_ERR_NO_MEM;
      }
      esp_gps->handlers[i]->stmt = strdup(handler->stmt);
      if (esp_gps->handlers[i]->stmt == NULL) {
          /* Failed to allocate memory */
          free(esp_gps->handlers[i]);
          return ESP_ERR_NO_MEM;
      }
      esp_gps->handlers[i]->handler = handler->handler;
      esp_gps->handlers[i]->required = handler->required;
      esp_gps->handlers[i]->object = handler->object;
      esp_gps->handlers[i]->context = handler->context;

      if (handler->required)
      {
        esp_gps->all_statements |= (1 << i);
      }

      ESP_LOGD(GPS_TAG, "[%d] registered handler for %s", i, handler->stmt);

      return ESP_OK;
    }
  }
  ESP_LOGE(GPS_TAG, "too many handlers registered");
  return ESP_ERR_NO_MEM;
}

/**
 * @brief Init NMEA Parser
 *
 * @param config Configuration of NMEA Parser
 * @return nmea_parser_handle_t handle of nmea_parser
 */
nmea_parser_handle_t nmea_parser_init(const nmea_parser_config_t *config)
{
  esp_gps_t *esp_gps = calloc(1, sizeof(esp_gps_t));
  if (!esp_gps) {
    ESP_LOGE(GPS_TAG, "calloc memory for esp_gps failed");
    goto err_gps;
  }
  esp_gps->buffer = calloc(1, NMEA_PARSER_RUNTIME_BUFFER_SIZE);
  if (!esp_gps->buffer) {
    ESP_LOGE(GPS_TAG, "calloc memory for runtime buffer failed");
    goto err_buffer;
  }

  nmea_parser_register_function(esp_gps, &(nmea_parser_handler_t){ "$GPGGA", &nmea_parse_gga, NMEA_STATEMENT_GGA_REQUIRED, &esp_gps->data, NULL });
  nmea_parser_register_function(esp_gps, &(nmea_parser_handler_t){ "$GPGSA", &nmea_parse_gsa, NMEA_STATEMENT_GSA_REQUIRED, &esp_gps->data, NULL });
  nmea_parser_register_function(esp_gps, &(nmea_parser_handler_t){ "$GPRMC", &nmea_parse_rmc, NMEA_STATEMENT_RMC_REQUIRED, &esp_gps->data, NULL });
  nmea_parser_register_function(esp_gps, &(nmea_parser_handler_t){ "$GPGSV", &nmea_parse_gsv, NMEA_STATEMENT_GSV_REQUIRED, &esp_gps->data, &esp_gps->sat });
  nmea_parser_register_function(esp_gps, &(nmea_parser_handler_t){ "$GPGLL", &nmea_parse_gll, NMEA_STATEMENT_GLL_REQUIRED, &esp_gps->data, NULL });
  nmea_parser_register_function(esp_gps, &(nmea_parser_handler_t){ "$GPVTG", &nmea_parse_vtg, NMEA_STATEMENT_VTG_REQUIRED, &esp_gps->data, NULL });

  /* Set attributes */
  esp_gps->uart_port = config->uart.uart_port;
  esp_gps->all_statements &= 0xFE;
  /* Install UART friver */
  uart_config_t uart_config = {
    .baud_rate = config->uart.baud_rate,
    .data_bits = config->uart.data_bits,
    .parity = config->uart.parity,
    .stop_bits = config->uart.stop_bits,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
  };
  if (uart_param_config(esp_gps->uart_port, &uart_config) != ESP_OK) {
    ESP_LOGE(GPS_TAG, "config uart parameter failed");
    goto err_uart_config;
  }
  if (uart_set_pin(esp_gps->uart_port, config->uart.tx_pin, config->uart.rx_pin,
                    UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE) != ESP_OK) {
    ESP_LOGE(GPS_TAG, "config uart gpio failed");
    goto err_uart_config;
  }
  if (uart_driver_install(esp_gps->uart_port, CONFIG_NMEA_PARSER_RING_BUFFER_SIZE, 0,
                          config->uart.event_queue_size, &esp_gps->event_queue, 0) != ESP_OK) {
    ESP_LOGE(GPS_TAG, "install uart driver failed");
    goto err_uart_install;
  }
  /* Set pattern interrupt, used to detect the end of a line */
  uart_enable_pattern_det_intr(esp_gps->uart_port, '\n', 1, 10000, 10, 10);
  /* Set pattern queue size */
  uart_pattern_queue_reset(esp_gps->uart_port, config->uart.event_queue_size);
  uart_flush(esp_gps->uart_port);
  /* Create Event loop */
  esp_event_loop_args_t loop_args = {
    .queue_size = NMEA_EVENT_LOOP_QUEUE_SIZE,
    .task_name = NULL
  };
  if (esp_event_loop_create(&loop_args, &esp_gps->event_loop_handle) != ESP_OK) {
    ESP_LOGE(GPS_TAG, "create event loop faild");
    goto err_eloop;
  }
  /* Create NMEA Parser task */
  BaseType_t err = xTaskCreate(
      nmea_parser_task_entry,
      "nmea_parser",
      CONFIG_NMEA_PARSER_TASK_STACK_SIZE,
      esp_gps,
      CONFIG_NMEA_PARSER_TASK_PRIORITY,
      &esp_gps->task_handle);
  if (err != pdTRUE) {
    ESP_LOGE(GPS_TAG, "create NMEA Parser task failed");
    goto err_task_create;
  }
  ESP_LOGI(GPS_TAG, "NMEA Parser init OK");
  return esp_gps;
  /*Error Handling*/
err_task_create:
  esp_event_loop_delete(esp_gps->event_loop_handle);
err_eloop:
err_uart_install:
  uart_driver_delete(esp_gps->uart_port);
err_uart_config:
err_buffer:
  free(esp_gps->buffer);
err_gps:
  free(esp_gps);
  return NULL;
}

/**
 * @brief Deinit NMEA Parser
 *
 * @param nmea_handle handle of NMEA parser
 * @return esp_err_t ESP_OK on success,ESP_FAIL on error
 */
esp_err_t nmea_parser_deinit(nmea_parser_handle_t nmea_handle)
{
  esp_gps_t *esp_gps = (esp_gps_t *)nmea_handle;
  vTaskDelete(esp_gps->task_handle);
  esp_event_loop_delete(esp_gps->event_loop_handle);
  esp_err_t err = uart_driver_delete(esp_gps->uart_port);
  free(esp_gps->buffer);
  free(esp_gps);
  return err;
}

/**
 * @brief Add user defined handler for NMEA parser
 *
 * @param nmea_handle handle of NMEA parser
 * @param event_handler user defined event handler
 * @param handler_args handler specific arguments
 * @return esp_err_t
 *  - ESP_OK: Success
 *  - ESP_ERR_NO_MEM: Cannot allocate memory for the handler
 *  - ESP_ERR_INVALIG_ARG: Invalid combination of event base and event id
 *  - Others: Fail
 */
esp_err_t nmea_parser_add_handler(nmea_parser_handle_t nmea_handle, esp_event_handler_t event_handler, void *handler_args)
{
  esp_gps_t *esp_gps = (esp_gps_t *)nmea_handle;
  return esp_event_handler_register_with(esp_gps->event_loop_handle, ESP_NMEA_EVENT, ESP_EVENT_ANY_ID, event_handler, handler_args);
}

/**
 * @brief Remove user defined handler for NMEA parser
 *
 * @param nmea_handle handle of NMEA parser
 * @param event_handler user defined event handler
 * @return esp_err_t
 *  - ESP_OK: Success
 *  - ESP_ERR_INVALIG_ARG: Invalid combination of event base and event id
 *  - Others: Fail
 */
esp_err_t nmea_parser_remove_handler(nmea_parser_handle_t nmea_handle, esp_event_handler_t event_handler)
{
  esp_gps_t *esp_gps = (esp_gps_t *)nmea_handle;
  return esp_event_handler_unregister_with(esp_gps->event_loop_handle, ESP_NMEA_EVENT, ESP_EVENT_ANY_ID, event_handler);
}