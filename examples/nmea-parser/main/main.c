#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "nmea_parser.h"

static const char *TAG = "nmea-parser";

#define TIME_ZONE (+8)   //Beijing Time
#define YEAR_BASE (2000) //date in GPS starts from 2000

/**
 * @brief GPS Event Handler
 *
 * @param event_handler_arg handler specific arguments
 * @param event_base event base, here is fixed to ESP_NMEA_EVENT
 * @param event_id event id
 * @param event_data event specific arguments
 */
static void gps_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
  gps_t *gps = NULL;
  switch (event_id)
  {
  case GPS_UPDATE:
    gps = (gps_t *)event_data;
    /* print information parsed from GPS statements */
    ESP_LOGI(TAG, "%02d.%02d.%02d %02d:%02d:%02d => \r\n"
                  "\t\t\t\t\t\tlatitude   = %.05f°N\r\n"
                  "\t\t\t\t\t\tlongtitude = %.05f°E\r\n"
                  "\t\t\t\t\t\taltitude   = %.02fm\r\n"
                  "\t\t\t\t\t\tspeed      = %fm/s",
             gps->date.year + YEAR_BASE, gps->date.month, gps->date.day,
             gps->time.hour + TIME_ZONE, gps->time.minute, gps->time.second,
             gps->latitude, gps->longitude, gps->altitude, gps->speed);
    break;
  case GPS_UNKNOWN:
    /* print unknown statements */
    ESP_LOGW(TAG, "Unknown statement:%s", (char *)event_data);
    break;
  default:
    break;
  }
}

void app_main()
{
  /* MTK3339 parser configuration */
  nmea_parser_config_t config = NMEA_PARSER_CONFIG_DEFAULT();
  /* init MTK3339 library */
  nmea_parser_handle_t handle = nmea_parser_init(&config);

  /* register event handler for NMEA parser library */
  nmea_parser_add_handler(handle, gps_event_handler, NULL);
}
